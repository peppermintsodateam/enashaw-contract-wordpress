$(document).ready(function(){
	//////////////////////////////////////////////////////////////
	// 	Functions
	//////////////////////////////////////////////////////////////
	
	// Check URL segments
	function getSeg(segNum){
		var newURL    = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
		var pathArray = window.location.pathname.split( '/' );
		var segment   = pathArray[segNum];
		
		return segment;	
	}
	//////////////////////////////////////////////////////////////
	// CTA menu
	//////////////////////////////////////////////////////////////
	$('.sub-nav li a').click(function(){
		
		var menu    = $('#cta-wrap');
		var active  = $(this);
		var pressed = active.attr('rel');
		var phone   = $('.sub-nav li a.phone');
		var mail    = $('.sub-nav li a.mail');
		
		if(pressed == 'phone'){
			phone.addClass('active');
			mail.removeClass('active');
			$('.phone-cta').show();
			$('.mail-cta').hide();
			
			if(!menu.hasClass('open')){
				menu.slideDown(300);
				menu.addClass('open');
			}
		}
		else if(pressed == 'mail'){
			mail.addClass('active');
			phone.removeClass('active');
			$('.phone-cta').hide();
			$('.mail-cta').show();
			
			if(!menu.hasClass('open')){
				menu.slideDown(300);
				menu.addClass('open');
			} 
		}

	});
	$('.close-cta').click(function(){
		closeCtaMenu()			
	});
	function closeCtaMenu() {
		var menu = $('#cta-wrap');
		menu.slideUp(300);
		menu.removeClass('open');
		$('.sub-nav li a').removeClass('active');	
	}
	//////////////////////////////////////////////////////////////
	// Homepage read more
	//////////////////////////////////////////////////////////////
	$('.show-more').click(function(){
		$('.more-content').fadeIn(300);
	});
	$('.show-less').click(function(){
		$('.more-content').fadeOut(300);
	});
	//////////////////////////////////////////////////////////////
	// Initate content slider
	//////////////////////////////////////////////////////////////
	$('.key-slide').bxSlider({
		mode: 'fade',
		speed: 800,
		pager: false,
		controls: false,
		auto: true,
		pause: 10000
	});
	//////////////////////////////////////////////////////////////
	// Initate fullscreen homepage image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.home-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	//////////////////////////////////////////////////////////////
	// Initate fullscreen manufacturing image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.manu-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	//////////////////////////////////////////////////////////////
	// Initate fullscreen case study image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.case-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	//////////////////////////////////////////////////////////////
	// Initate fullscreen contact image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.contact-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	//////////////////////////////////////////////////////////////
	// Initate fullscreen generic image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.generic-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	//////////////////////////////////////////////////////////////
	// Initate fullscreen 404 image
	//////////////////////////////////////////////////////////////
	(function(){
		// Global slider's DOM elements
        var $example = $('.fourofour-slider'),
            $frame   = $('.frame', $example);

        // Calling mightySlider via jQuery proxy
        $frame.mightySlider({
            viewport: 'fill',
                
            // Navigation options
            navigation: {
                slideSize: '100%'
            },
			
			dragging: {
				dragSource:    null, // Selector or DOM element for catching dragging events. Default is FRAME.
				mouseDragging: 0
			},

            // Commands options
            commands: {
                pages: 0,
                buttons: 0
            }
        });
    })();
	
	//////////////////////////////////////////////////////////////
	// Case studies
	//////////////////////////////////////////////////////////////
	$(".location-drop").click(function(){
		var id   = $(this).attr('rel');
		var div  = $('#'+id);
		var drop = $('#'+id+' ul');
		
		if(div.hasClass('hide')){
			drop.slideDown(600);
			div.removeClass('hide');
		} else {
			drop.slideUp(600);
			div.addClass('hide');	
		}
	});
	//////////////////////////////////////////////////////////////
	// News
	//////////////////////////////////////////////////////////////
	
	// Add tool tips to news thumbnails
	$('.tip').tipsy({gravity: 'n'});
				
	// Initiate reponsive grid		
	$(".news-wrap").gridalicious({
		gutter: 30, 
		width: 300, 
		animate: true, 
		animationOptions: { 
			speed: 200, 
			duration: 300
		}
	});
	
	// Function to load new new articles onto the grid
	function loadNews(){
		// Compile new articles
		insertNews = function(){
			
			var current  = $('#current_limit').val();
			var category = $('#current_cat').val();
			var limit    = +current + +6;
			var articles = new Array;
			
			// Remove button and show loading
			if(/iPhone/i.test(navigator.userAgent)){$('#more-posts').hide();}
			$('#more-loading').show();
			
			// Get new articles via json
			$.ajax({url: 'http://enashawcontract.co.uk/_includes/get_news', async: false, dataType: 'json', data: {current:current, limit:limit, category:category}, success: function (data) {
				
				// For each entry returned
				for(i=0; i < data.length; i++){
					
					// define article content
					var date    = data[i]["date"];
					var excerpt = data[i]["excerpt"];
					var href    = data[i]["href"];
					var image   = data[i]["image"];
					var title   = data[i]["title"];
					
					// Create each article variable
					var article = '<div class="item">';
					
					if(image != ''){
						var article = article + '<a href="' + href +'"><img src="' + image + '" alt="' + title + '" /></a><div class="trans transparent"></div>';
					}
					
					var article = article + '<div class="article"><div class="title"><a href="' + href +'">' + title + '</a></div><p>' + excerpt + '</p><p class="timeago">' + date + '</p></div><div class="cleaner"></div></div>';
					
					// Push each article to the articles array
					articles.push(article);
						
				}
				
				if(i<6){
					$('#more-posts').hide();
					$('#posts-ended').fadeIn(500);		
				}
			}});
			
			// Change current variable on page
			$('#current_limit').val(limit);
			
			// Remove loding and show button
			$('#more-loading').hide();
			if(/iPhone/i.test(navigator.userAgent)){$('#more-posts').show();}
			
			// Return the articles array
			return articles;
			
		}
		
		// Add new articles to the grid	
		$('.news-wrap').gridalicious('append', insertNews());	
	}
	
	// If news page, add scroll action to add news articles
	if(getSeg(1) == 'news'){
		// Load news articles when user is 300 pixels from the bottom of the window
		window.onscroll = function() {
			// If you're at the bottom of the document, load new articles
			if ($(window).scrollTop() >= $(document).height() - $(window).height() - 350) {loadNews();}
		}
		
		// if userAgent is and iPhone, show the more posts button
		if(/iPhone/i.test(navigator.userAgent)){$('#more-posts').show();}
		
		// Load news articles when button is pressed
		$('#more-posts').click(function(){loadNews();});
	}
	
	//////////////////////////////////////////////////////////////
	// Get in touch
	//////////////////////////////////////////////////////////////				
	function escMap() {
		var c = new google.maps.LatLng(53.41800629999999, -2.738459799999987);
		var a = {
			zoom: 16,
			center: c,
			mapTypeControl: true,
			mapTypeControlOptions: {
			  style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			navigationControl: true,
			navigationControlOptions: {
				style: google.maps.NavigationControlStyle.SMALL
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var d = new google.maps.Map(document.getElementById("map"), a);
		var f = new google.maps.MarkerImage("http://www.farjo.net/new_website/_assets/_img/contact-map-pin.png", new google.maps.Size(100, 50), new google.maps.Point(0, 0), new google.maps.Point(50, 50));
		var h = new google.maps.MarkerImage("http://www.farjo.net/new_website/_assets/_img/contact-map-shadow.png", new google.maps.Size(130, 50), new google.maps.Point(0, 0), new google.maps.Point(65, 50));
		var g = new google.maps.LatLng(53.41800629999999, -2.738459799999987);
		var b = new google.maps.Marker({
			position: g,
			map: d,
			icon: f,
			shadow: h,
			title: "Ena Shaw Contract",
			zIndex: 3
		});
	}
});
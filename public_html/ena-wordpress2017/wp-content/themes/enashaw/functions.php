<?php 

/*--------------------------------
        THEME CONFIGURATION
--------------------------------*/
define('THEME_PATH', get_template_directory_uri());

/*--------------------------------
   DEFINE PATH (DIFFERENT METHOD)
--------------------------------*/

if(!defined('ENA_THEME_DIR')) {
	define('ENA_THEME_DIR', get_theme_root().'/'.get_template().'/');
}


if(!defined('ENA_THEME_DIR')) {
	define('ENA_THEME_DIR', WP_CONTENT_URL.'/themes/'.get_template().'/');
}

require_once ENA_THEME_DIR.'libs/posttypes.php';


/*--------------------------------
        LOAD JQUERY
--------------------------------*/

function jquery_scripts_method() {
    wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'jquery_scripts_method' );


// Defer Javascripts
// Defer jQuery Parsing using the HTML5 defer property
if (!(is_admin() )) {
    function defer_parsing_of_js ( $url ) {
        if ( FALSE === strpos( $url, '.js' ) ) return $url;
        if ( strpos( $url, 'jquery.js' ) ) return $url;
        // return "$url' defer ";
        return "$url' defer onload='";
    }
    add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
}


/*--------------------------------
    CUSTOM JQUERY PLUGINS
--------------------------------*/
function plugins_enqueue() {
	wp_register_script( 'tween_max','https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js', array('jquery'),"",  true );

	wp_register_script( 'scroll_magic','http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array('jquery'),"",  true );

	wp_register_script( 'scroll_debugger','http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', array('jquery'),"",  true );
	wp_register_style('font_awesome','http://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), null, 'all');


  wp_register_style('crimson_font','https://fonts.googleapis.com/css?family=Crimson+Text:400,600,700" rel="stylesheet', array(), null, 'all');
  wp_register_style('montserrat_font','https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800" rel="stylesheet', array(), null, 'all');

  wp_register_script( 'map_api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD1R3i4DtcBc-GRi25mtYNkPs5xksN-WOE', array('jquery'),"",  false);
	wp_register_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'),"",  false);
	wp_register_script( 'gsap', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', array('jquery'),"1.0",  true);
	wp_register_script( 'images_loaded','https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.min.js', array('jquery'),"",  true );

  wp_register_script( 'mixup', get_template_directory_uri() . '/js/vendor/mixitup.min.js', array('jquery'), "1.0",  true );
  wp_register_script( 'ajax_js', get_template_directory_uri() . '/js/vendor/ajax-load.js', array('jquery'), "1.0",  true );
  wp_register_script( 'owl_carousel', get_template_directory_uri() . '/js/vendor/owl.carousel.min.js', array('jquery'),"1.0",  true );

  wp_register_script( 'map', get_template_directory_uri() . '/js/vendor/google-map.js', array('jquery'), "1.0",  true );


	wp_register_script( 'case_js', get_template_directory_uri() . '/js/case-studies.js', array('jquery'), "1.0",  true );
  wp_register_script( 'main_js', get_template_directory_uri() . '/js/main.js', array('jquery'), "1.0",  true );

	wp_register_style('screen', get_template_directory_uri() .'/css/plugins.css', array(), null, 'all');
	wp_register_style('main', get_template_directory_uri() .'/css/main.css', array(), null, 'all');

	wp_enqueue_script('tween_max');
	wp_enqueue_script('scroll_magic');
	wp_enqueue_script('scroll_debugger');
	wp_enqueue_script('images_loaded');

	wp_enqueue_script('modernizr');
	wp_enqueue_script('gsap');

	/*if(is_front_page()) {
		wp_enqueue_script('canvas_js');
	}*/


if(is_page(83)) {
  wp_enqueue_script('mixup');
}

if(is_page(106)) {
     wp_enqueue_script('map_api');
  }
 
  if(is_page(106)) {
     wp_enqueue_script('map');
  }

wp_enqueue_script('mixup');

if(is_page(7)) {
  wp_enqueue_script('ajax_js');
  wp_deregister_script( 'main_js');
}

  if (is_home()) {
    wp_enqueue_script('ajax_js');
    wp_deregister_script( 'main_js');
    /* wp_deregister_script( 'mixup');*/
    
}


wp_enqueue_script('owl_carousel');

wp_enqueue_script('main_js');

if (is_page('26') || is_page('28') || is_page('30') || is_page('32') || is_page('34')) {
  wp_enqueue_script('case_js');
  wp_deregister_script( 'main_js');
}

wp_enqueue_style('montserrat_font');
wp_enqueue_style('crimson_font');
wp_enqueue_style('font_awesome');
wp_enqueue_style('screen');
wp_enqueue_style('main');

}


add_action( 'wp_enqueue_scripts', 'plugins_enqueue' );


/* Remove Emoji Styles & Scripts*/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/*--------------------------------
        REMOVE ADMIN BAR
--------------------------------*/
add_filter('show_admin_bar', '__return_false');


/*--------------------------------
        <p> AND <br> TAGS SPACES
--------------------------------*/
function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}

add_filter('the_content', 'remove_empty_p', 20, 1);


remove_filter( 'the_sidebar', 'wpautop' );

remove_filter( 'the_excerpt', 'wpautop' );

/*--------------------------------
 FEATURED IMAGES IN DASHBOARD SUPPORT
--------------------------------*/

function check_thumbnail_support() {  
 if (!current_theme_supports('post-thumbnails')) {  
   add_theme_support( 'post-thumbnails' );  
   //add_action('init','remove_posttype_thumbnail_support');  
 }  
}  
add_action('after_setup_theme','check_thumbnail_support');



function get_ajax_page() {

    header( "Content-Type: application/json" );
    $id_post = $_POST['post_id'];
    $post_att = get_post($id_post);

    $url_post = get_permalink($id_post);
    $post_att->the_permalink = $url_post;

    echo json_encode($post_att, JSON_UNESCAPED_UNICODE);
}

add_action('wp_ajax_nopriv_get-content-page', 'get_ajax_page');
add_action('wp_ajax_get-content-page', 'get_ajax_page');


/*--------------------------------
 IMAGE SIZE CUSTOM FUNCTION
--------------------------------*/
add_image_size('custom-image', 1920, 1080, true);
add_image_size('team-image', 250, 250, true);
add_image_size('footer-image', 9999, 200, true);
add_image_size('testimonial-img', 9999, 70, true);


// Removing <p> tags from ACF text editor
remove_filter ('acf_the_content', 'wpautop');

/*--------------------------------
 NAVIGATION MAIN
--------------------------------*/
add_action( 'init', 'my_custom_menus' );
function my_custom_menus() {
    register_nav_menus(
        array(
            'main-menu' => __( 'Main Menu' ),
            'icaw-menu' => __( 'Secondary menu' )
        )
    );
}


/*--------------------------------
  CLASSES TO PREV NEXT BUTTON
--------------------------------*/

add_filter('next_post_link', 'post_link_attributes_next');
add_filter('previous_post_link', 'post_link_attributes_prev');
 
function post_link_attributes_next($output) {
    $code = 'class="cta-btn read-more-pag next-btn"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}

function post_link_attributes_prev($output) {
    $code = 'class="cta-btn read-more-pag prev-btn"';
    return str_replace('<a href=', '<a '.$code.' href=', $output);
}


/*--------------------------------
 EXCERPT FUNCTION
--------------------------------*/

function the_excerpt_max_charlength($charlength) {
    echo cutText(get_the_excerpt(), $charlength);
  }


    function cutText($text, $maxLength){
        
        $maxLength++;

        $return = '';
        if (mb_strlen($text) > $maxLength) {
            $subex = mb_substr($text, 0, $maxLength - 5);
            $exwords = explode(' ', $subex);
            $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
            if ($excut < 0) {
                $return = mb_substr($subex, 0, $excut);
            } else {
                $return = $subex;
            }
            $return .= '[...]';
        } else {
            $return = $text;
        }
        
        return $return;
    }




?>
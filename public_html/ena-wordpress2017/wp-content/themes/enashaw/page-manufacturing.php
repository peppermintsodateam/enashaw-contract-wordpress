<?php 
  /*
    Template Name: Page Manufacturing
  */
?>


<?php get_header('case'); ?>


    <div id="cases-slider" class="case-rotator">

        <div id="slider-wrapper" class="case-container">

           <?php

                $args = array(

                    'post_type'=>'manufacturing_case',
                    'posts_per_page'=> -1,
                    'post_status' => 'publish'

                );

                     $loop_case = new WP_Query($args);

                 ?>


                 <?php if($loop_case->have_posts() ) : ?>

                    <?php while ($loop_case->have_posts() ) : $loop_case->the_post(); ?>
                    
                        <section class="slide-case">
                            <div class="case-bck-container backgrounds">

                                <div class="bck-container">

                                    <div class="container-slides absolute-pos">

                                    <div class="scroll-down-arrow">
                                        <div data-scroll="content-scroll" class="arrow-down-inner"></div>
                                      </div>

                                        <div class="big-bck-wrapper">
                                            <?php echo get_template_part('loops/loop','bck'); ?>
                                        </div>

                                    </div>


                                    <div class="container-texts absolute-pos">

                                        <div class="all-content-wrapper">
                                            <div class="case-content-inner">

                                                <div class="case-column case-left">
                                                    <div class="case-header-badge">

                                                        <div class="case-btn-header">
                                                             <h3 class="inner-case-header"><?php the_title(); ?> </h3>
                                                        </div>

                                                        <div class="arrow-container">
                                                           <div class="arrow-back-wrapper">
                                                               <div class="arrow-inner"></div>
                                                           </div>
                                                        </div>
                                                    </div>

                                                    <div class="case-text-wrapper content-scroll">

                                                        <div class="text-inner-case flex-inner">
                                                            

                                                            <h2 class="case-text-header">
                                                                <?php the_field('title_case') ?>
                                                            </h2>

                                                           <p><?php the_field('left_column_text'); ?></p>

                                                           
                                                        </div>

                                                         <div class="text-inner-case text-center-flex flex-inner">
                                                          <p> <?php the_field('right_column_text'); ?></p>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="case-column case-right">

                                                   <div class="btn-column flex-item">
                                                       <div class="property-title">
                                                           <div class="tab-outside">
                                                               <div class="tab-caption"><?php the_title(); ?></div>
                                                           </div>
                                                       </div>
                                                   </div> 

                                                   <div class="gallery-wrapper flex-item">
                                                      <div class="thumbnail-case-wrapper">
                                                        

                                                        <div class="all-thumb-wrapper">
                                                            <?php echo get_template_part('loops/loop','thumbs'); ?>

                                                        </div>
                                                    </div>
                                                   </div>

                                                </div>
                                            </div><!--End case content inner-->

                                        </div>

                                    </div>
                                    
                                </div>

                            </div>
                    
                        </section><!--End of 1st slide container-->

                    <?php endwhile; ?>

                <?php endif; wp_reset_query(); ?>

                


           </div><!--End of case container-->

           <nav class="bottom-pagination-slider">
               <div class="pagination-inner">

                    <div class="counter-diamond flex-item">
                        <div class="bullet-counter">
                            <div class="bullet-text"></div>
                        </div>
                    </div>

                    <div class="line-flex-wrapper flex-item"></div>

                    <div class="pags-container flex-item">
                       
                    </div>
               </div>
           </nav>

           <div class="footer-arrows">
               <div class="footer-arrows-inner">
                   <div class="arrows-inner arrow-inner-left"></div>
                   <div class="arrows-inner arrow-inner-right"></div>
               </div>
           </div>

        </div><!--End of case rotator-->

<?php get_footer(); ?>
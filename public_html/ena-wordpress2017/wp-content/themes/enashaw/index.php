<?php get_header(); ?>


    
    <div class="main-page">

            <?php echo get_template_part('parts/header','blog'); ?>

            <section id="blog" class="section">
                <div class="page-wrapper"> 
                    <div class="pos-center">

                        <div class="row-spacer">

                        		<?php
                                    $args = array(

                                        'post_type'=>'post',
                                        'order'=>'DESC',
                                        'orderby' => 'post_date',
                                        'showposts'=>4,

                                    );

                                    $newest_posts = new WP_query($args);
                                 ?>

							 <?php if($newest_posts->have_posts() ) :  ?>

                            <ul style="display:none;" class="filter-flex-list">

                            <li class="filter case-btn all-btn-test" data-filter="*"><span>All</span></li>
                             	

                             	<?php 
                                        $terms = get_terms('category');
                                        $count = count($terms);

                                        if($count > 0) {
                                            foreach($terms as $term) {
                                               // echo "<li><a class='filter-btn' href='#' data-sortr='.".$term->slug."'>" . $term->name . "</a></li>";
                                                echo "<li class='filter test-btn' data-filter='".$term->slug."'><span>" . $term->name . "</span></li>";
                                            }
                                        }
                                    ?>

                        	</ul>

                        	<?php endif; wp_reset_query(); ?>

                        </div>

                         <section id="news-section" class="blog-section">

                         </section>
                    </div>
                </div>
            </section>

             <footer class="footer-page-logo">
                <div class="page-wrapper">
                    <div class="pos-center">
                       
                        <section class="footer-logos flex-section owl-carousel">

                            <?php echo get_template_part('loops/footer','loop'); ?>

                        </section>
                    </div>
                </div>
            </footer>   
            

        </div>


<?php get_footer(); ?>


    
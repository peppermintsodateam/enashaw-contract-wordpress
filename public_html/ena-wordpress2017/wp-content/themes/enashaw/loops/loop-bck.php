    

   <?php if(have_rows('hero_image') ) : ?>
        <?php while(have_rows('hero_image') ) : the_row(); ?>

        	<?php
				$imgID = get_sub_field('bck');
				$imgSize = "full"; // (thumbnail, medium, large, full or custom size)
				$imgArr = wp_get_attachment_image_src( $attachid, $size );
				// url = $image[0];
				// width = $image[1];
				// height = $image[2];
			?>
        	
            <div class="top-bck bcg" style="background-image: url(<?php echo $imgID['url']; ?> );"></div>
        <?php endwhile; ?>
    <?php endif; ?>
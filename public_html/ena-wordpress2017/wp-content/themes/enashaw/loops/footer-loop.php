
<?php 
	$args = array(

		'posts_per_page'=>-1,
		'post_type'=>'footer_logo'

	);

	$footer_loop = new WP_Query($args);
?>

<?php if($footer_loop->have_posts() ) : ?>

	<?php while($footer_loop->have_posts() ) :  $footer_loop->the_post(); ?>
		 <figure class="logo-bottom">
		 	<?php 
		 		if(has_post_thumbnail()) {
		 			the_post_thumbnail('footer-image', array('alt'=>get_the_title())); 

		 		}
		 	 ?>
		 </figure>
	<?php endwhile; ?>

	<?php endif; ?>


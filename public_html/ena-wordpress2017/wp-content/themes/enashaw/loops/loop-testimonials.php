<?php 

	$args = array(

        'post_type'=>'testimonials_ena',
        'posts_per_page'=>-1,
        'order'=>'DESC',
        'orderby' => 'post_date'
    );

    $testimonials_loop = new WP_Query($args);

?>

<?php if($testimonials_loop->have_posts() ) : ?>

	<?php while($testimonials_loop->have_posts() ) : $testimonials_loop->the_post(); 

			$termsArray = get_the_terms( $post->ID, "testimonials_category" );
            $termsString = "";

             foreach ( $termsArray as $term ) { // for each term 
            $termsString .= $term->slug.' '; //create a string that has all the slugs 

        }

	?>


	<article class="testimonial-article mix <?php echo $termsString; ?>">
		 <div class="article-inner">
		 	<h2 class="testimonial-header">
		 		<?php the_field('header_top'); ?>
		 	</h2>

		 	<p class="content-test">
		 		<?php the_field('main_content') ?>
		 	</p>
		 	
			  <p class="author-content">
			  	<?php the_field('author_content') ?>
			  </p>

			  <footer class="test-footer">
		             <figure class="test-logo">
							
						<?php 
					 		if(has_post_thumbnail()) {
					 			the_post_thumbnail('testimonial-img', array('alt'=>get_the_title())); 

					 		}
					 	 ?>

		            </figure>
	           </footer>

		 </div>
	</article>



	<?php endwhile; ?>

<?php endif; wp_reset_query(); ?>
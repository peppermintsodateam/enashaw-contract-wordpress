<?php if(have_rows('who_we_are') ) : ?>

    <?php while(have_rows('who_we_are') ) : the_row(); ?>

        <?php

             $image = get_sub_field('feature_image');

                if($image) {
                  echo wp_get_attachment_image($image);
                }

              ?>


           <div class="image-column flex-item fadeIn">
               <figure class="feature-img bcg" style="background-image: url('<?php echo $image; ?>')" ></figure>
           </div>

        <div class="text-column flex-item fadeInBottom">
            <h2 class="pages-headers grey-header"><?php the_sub_field('header_page'); ?></h2>

                 <?php the_sub_field('desc_text'); ?>
           </div>

    <?php endwhile; ?>

<?php endif; ?>
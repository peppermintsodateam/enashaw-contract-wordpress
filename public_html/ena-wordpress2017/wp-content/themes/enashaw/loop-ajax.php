<?php 
  define('WP_USE_THEMES', false);
    require_once('../../../wp-load.php');


    //echo dirname(__FILE__); - wyświetla katalog z danym plikiem

    if(isset($_GET['GETposts'])) {
        $GETposts = $_GET['GETposts'];
    }else {
        $GETposts = 0;
    }


    if(isset($_GET['GETpage'])) {
        $GETpage = $_GET['GETpage'];
    }else {
        $GETpage = 0;
    }


    $args = array(

        'post_type'=>'post',
        'posts_per_page'=>$GETposts,
        'order'=>'DESC',
        'orderby' => 'post_date',
        'paged'=>$GETpage
    );

    $posts_loop = new WP_Query($args);
?>



<?php if($posts_loop->have_posts() ) : ?>
     <?php while($posts_loop->have_posts() ) : $posts_loop->the_post(); 
            $termsArray = get_the_terms( $post->ID, "category" );
            $termsString = ""; 

             foreach ( $termsArray as $term ) { // for each term 
            $termsString .= $term->slug.' '; //create a string that has all the slugs 
        }

    ?>

    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'custom-image' );?>

            <article class="blog-article mix <?php echo $termsString; ?>">
                 <div class="article-inner-blog">
                     <div class="blog-article-content bck-f bcg" style="background-image: url('<?php echo $thumb['0'];?>')">
                         <div class="absolute-inner">
                             <div class="blog-flex-wrapper">
                                 <div class="blog-item-wrapper">

                                     <h3 class="blog-title">
                                         <?php the_title(); ?>
                                    </h3>

                                    <span class="date"><?php echo get_the_date('M jS, Y'); ?></span>
                                    <a href="<?php the_permalink(); ?>" class="cta-blog read-more">Read more</a>

                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
            </article>

        <?php endwhile; ?>
<?php endif; wp_reset_query(); ?>
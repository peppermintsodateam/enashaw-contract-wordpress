<?php 
/*
    Template Name: Testimonials Page
*/
?>

<?php get_header(); ?>

        <div class="main-page">

             <?php get_template_part('parts/header','page'); ?>

            <section id="testimonials" class="section filter-section">
                <div class="page-wrapper">
                    <div class="pos-center">



                    <?php
                            $args = array(

                                'post_type'=>'testimonials_ena',
                                'order'=>'DESC',
                                'orderby' => 'post_date',
                                'showposts'=>5
                        );

                            $newest_test = new WP_query($args);
                        ?>

                         <?php if($newest_test->have_posts() ) :  ?>

                        <div class="row-spacer">
                                <ul class="filter-flex-list">

                                    <li class="filter-btn case-btn all-btn-test" data-filter="*"><span>All testimonials</span></li>



                                     <?php 
                                                $terms = get_terms('testimonials_category');
                                                $count = count($terms);

                                                if($count > 0) {
                                                    foreach($terms as $term) {
                                                       // echo "<li><a class='filter-btn' href='#' data-sortr='.".$term->slug."'>" . $term->name . "</a></li>";
                                                        echo "<li class='filter-btn test-btn' data-filter='.".$term->slug."'><span>" . $term->name . "</span></li>";
                                                    }
                                                }
                                            ?>
                                
                            </ul>
                        </div>

                         <?php endif; wp_reset_query(); ?>



                        <section class="testimonials-section flex-section">
                            <?php get_template_part('loops/loop','testimonials'); ?>
                        </section>

                    </div>
                </div>
            </section>

             <footer class="footer-page-logo">
                <div class="page-wrapper">
                    <div class="pos-center">
                        <section class="footer-logos flex-section owl-carousel">

                            <?php echo get_template_part('loops/footer','loop'); ?>

                        </section>
                    </div>
                </div>
            </footer> 
            
        </div>

       

<?php get_footer(); ?>

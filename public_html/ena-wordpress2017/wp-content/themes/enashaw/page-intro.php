
<?php 
    /*
    Template Name: Home intro
    */
?>

<?php get_header('home'); ?>

<div id="move-slider" class="rotator">
            <div id="main-wrapper" class="slides-container">

                <?php

                    $args = array(

                        'post_type'=>'home_slider',
                        'posts_per_page'=> 6,
                        'post_status' => 'publish'

                    );

                     $loop_home = new WP_Query($args);

                 ?>

                  <?php if($loop_home->have_posts() ) : ?>
                     <?php while ($loop_home->have_posts() ) : $loop_home->the_post(); ?>

                        

                        <section class="slide">
                            <?php 
                                if(has_post_thumbnail()) {
                                   //$feature_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                  $feature_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'custom-image');
                                }
                            ?>

                             <div class="slide-bck bcg" style="background-image:url(<?php echo $feature_image[0]; ?>);">
                                  <div class="mask"></div>

                                     <?php 
                                         $post_id = get_the_ID();

                                         if ($post_id === 36) {
                                             get_template_part('parts/content','slide');

                                        }else if($post_id === 12) {
                                         get_template_part('parts/content','slides');

                                        }else if($post_id === 13) {
                                          get_template_part('parts/content','slides');
                                        }else if($post_id === 14) {
                                         get_template_part('parts/content','slides');

                                        }else if($post_id === 15) {
                                          get_template_part('parts/content','slides');

                                        }else if($post_id === 16) {
                                         get_template_part('parts/content','buttons');
                                        }

                                    ?>

                             </div>

                        </section>


                    <?php endwhile; ?>

                <?php endif; wp_reset_query(); ?>

            </div>

            <nav id="nav-container">
                <div class="list-bottom-container">

                <ul class="home-pagination" id="home-pagination">

                    <?php if(have_rows('pagination') ) : ?>

                        <?php while(have_rows('pagination') ) : the_row(); ?>

                            <li class="diamonds">
                                <span class="diamond-number">
                                    <div class="bullet-number"><?php the_sub_field('number'); ?></div>
                                </span>
                                <p class="pag-text <?php the_sub_field('button_class'); ?>"><?php the_sub_field('title'); ?></p>
                            </li>

                        <?php endwhile; ?>

                    <?php endif; ?>

                    </ul>

                    
                </div>
            </nav>


            <section class="small-device-nav">
                <div class="small-divice-inner">
                    <div class="arrows arrow-left"><span>Prev</span></div>
                    <div class="arrows arrow-right"><span>Next</span></div>
                </div>
            </section>


        </div>

        <?php get_footer(); ?>


    
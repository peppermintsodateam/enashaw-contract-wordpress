
<?php 
  /*
    Template Name: Case studies
  */
?>


<?php get_header('case'); ?>


    <div id="cases-slider" class="case-rotator">

           <div id="slider-wrapper" class="case-container">

                <section class="slide-case">
                    <div class="case-bck-container backgrounds">

                        <div class="bck-container">

                            <div class="container-slides absolute-pos">
                                <div class="big-bck-wrapper">
                                    <div class="top-bck inside-1 bcg"></div>
                                    <div class="top-bck inside-2 bcg"></div>
                                    <div class="top-bck inside-3 bcg"></div>
                                    <div class="top-bck inside-4 bcg"></div>
                                </div>
                            </div>


                            <div class="container-texts absolute-pos">

                                <div class="all-content-wrapper">
                                    <div class="case-content-inner">

                                        <div class="case-column case-left">
                                            <div class="case-header-badge">

                                                <div class="case-btn-header">
                                                    <h3 class="inner-case-header">Hospitality</h3>
                                                </div>

                                                <div class="arrow-container">
                                                   <div class="arrow-back-wrapper">
                                                       <div class="arrow-inner"></div>
                                                   </div>
                                                </div>
                                            </div>

                                            <div class="case-text-wrapper">

                                                <div class="text-inner-case flex-inner">
                                                    <h2 class="case-text-header">
                                                        <span>Lorem ipsum dolor sit amet</span>
                                                        <span> Illum, amet doloremque incidunt</span>
                                                    </h2>

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et laudantium sunt aperiam ratione. Nostrum, corrupti blanditiis repellat. Delectus harum laboriosam fugit vel, dignissimos animi accusantium repellat hic. Voluptatem, nobis ullam.</p>
                                                </div>

                                                 <div class="text-inner-case text-center-flex flex-inner">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et laudantium sunt aperiam ratione. Nostrum, corrupti blanditiis repellat. Delectus harum laboriosam fugit vel, dignissimos animi accusantium repellat hic. Voluptatem, nobis ullam.
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et laudantium sunt aperiam ratione. Nostrum, corrupti blanditiis repellat. Delectus harum laboriosam fugit vel, dignissimos animi accusantium repellat hic. Voluptatem, nobis ullam</p>
                                                </div>


                                            </div>

                                        </div>

                                        <div class="case-column case-right">

                                           <div class="btn-column flex-item">
                                               <div class="property-title">
                                                   <div class="tab-outside">
                                                       <div class="tab-caption">
                                                           Hotel Marriot
                                                       </div>
                                                   </div>
                                               </div>
                                           </div> 

                                           <div class="gallery-wrapper flex-item">
                                              <div class="thumbnail-case-wrapper">
                                                

                                                <div class="all-thumb-wrapper">

                                                    <div class="img-wrapper">
                                                        <div class="thumb-bck thumb-1 bcg"></div>
                                                    </div>

                                                    <div class="img-wrapper">
                                                        <div class="thumb-bck thumb-2 bcg"></div>
                                                    </div>

                                                    <div class="img-wrapper">
                                                        <div class="thumb-bck thumb-3 bcg"></div>
                                                    </div>

                                                    <div class="img-wrapper">
                                                        <div class="thumb-bck thumb-4 bcg"></div>
                                                    </div>
                                                </div>
                                            </div>
                                           </div>

                                        </div>
                                    </div><!--End case content inner-->
                                </div>

                            </div>
                            
                        </div>

                    </div>
                    
                </section><!--End of 1st slide container-->


           </div><!--End of case container-->

           <nav class="bottom-pagination-slider">
               <div class="pagination-inner">

                    <div class="counter-diamond flex-item">
                        <div class="bullet-counter">
                            <div class="bullet-text"></div>
                        </div>
                    </div>

                    <div class="line-flex-wrapper flex-item"></div>

                    <div class="pags-container flex-item">
                       
                    </div>
               </div>
           </nav>

           <div class="footer-arrows">
               <div class="footer-arrows-inner">
                   <div class="arrows-inner arrow-inner-left"></div>
                   <div class="arrows-inner arrow-inner-right"></div>
               </div>
           </div>

        </div><!--End of case rotator-->

<?php get_footer(); ?>

<?php get_header(); ?>



   <section id="error-content" class="bcg">

            <a href="<?php echo esc_url(home_url('/')); ?>" class="logo-content flex-error">
                 <img class="logo-white" src="<?php echo THEME_PATH; ?>/gfx/main-logo-w.svg" alt="">
            </a>

            <div class="error-message flex-error">
                <h1 class="error-header">
                    404 Page not found
                </h1>
            </div>

            <div class="text-message flex-error">
                <p>
                    <span>The link you clicked may be broken or</span>
                    <span>the page may have been removed.</span>
                </p>
            </div>

             <div class="links-content flex-error">
                 <p>Visit the <a href="<?php echo esc_url(home_url('/')); ?>">Homepage</a> or <a href="<?php echo get_page_link(106); ?>">Contact us</a> about the problem</p>
             </div>
              

        </section>



<?php get_footer(); ?>


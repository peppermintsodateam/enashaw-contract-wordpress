<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js" <?php language_attributes('html'); ?>> <!--<![endif]-->
    <head>

        <meta charset="<?php bloginfo('charset') ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        
     

       <?php 
          if((is_search())) : ?>
            <meta name="robots" content="noindex, nofollow" />
        <?php endif; ?>

         <title>

            <?php 
                if(is_archive()) {
                    echo ucfirst(trim(wp_title('',false))) . ' - ';
                }else 

                if(!(is_404() ) && (is_single()) || (is_page() || (is_home() ))) {
                    $title = wp_title('', false);

                    if(!empty($title)) {
                        echo $title . ' - ';
                    }
                }else 

                if(is_404()) {
                     echo 'Page can not be found!';
                }

                if(is_front_page()) {
                    bloginfo('name');
                    echo ' - ';
                    bloginfo('description');
                } else {
                    echo  bloginfo('name');
                }


                global $paged;

                if($paged > 1) {
                    echo ' - page ' . $paged;
                }


            ?>

        </title>

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" >

       

        <script>
          var baseUrl = "<?php echo get_template_directory_uri(); ?>";
          var postsCount = "<?php echo get_option('posts_per_page'); ?>"
        </script>

      <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69024726-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-69024726-1');
        </script>

      <?php wp_head(); ?>
    
    </head>

    <body <?php body_class( array( "home-slider is-loading")); ?>>
    
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

       <div id="preloader">
            <div class="intro-logo-container">
                <figure class="logo-container"></figure>
            </div>
        </div>


        <header class="header-bar fixed-top-header">

            <div class="logo-wrapper flex-top">
               <div class="main-logo-container">
                   <a href="index.html" class="main-logo-link">
                       <img src="<?php echo THEME_PATH; ?>/gfx/enashaw-logo-main.svg" alt="Ena Shaw Logo">
                   </a>
               </div>
            </div>

            <div class="switcher-wrapper flex-top">
                <div data-switch="a"  class="switcher-top nav-toggle">
                    <span class="slice slice1"></span>
                    <span class="slice slice2"></span>
                    <span class="slice slice3"></span>
                </div>
            </div>
        </header>



       


        <div class="navigation-hidden-switcher">
        
            <div class="main-nav-wrapper">

                <!-- Main nav goes here -->
                <div class="nav-container">

                    <div class="background"></div>

                    <div class="outer-flex">

                        <div class="inner-flex">
                            <div class="menu-container">

                              <?php echo get_template_part('parts/nav','main') ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
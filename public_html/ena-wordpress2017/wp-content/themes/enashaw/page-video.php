<?php 
/*
    Template Name: Video Page
*/
?>

<?php get_header(); ?>


	<div class="main-page">

            <div class="video-container">
               <div class="video-flex videoWrapper js-videoWrapper">

                  <?php the_field('video_content'); ?>

                  <?php if ( has_post_thumbnail() ) {
 
                                // Get the post thumbnail URL
                                $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

                              } else {
                            
                                // Get the default featured image in theme options
                                $feat_image = get_field('default_featured_image', 'option');
                            } ?>

                    <button class="videoPoster js-videoPoster bcg" style="background-image: url(<?php echo $feat_image; ?>);">Play video</button>

                    <!--<div id="video-controls">
                      <button type="button" id="play-pause" class="play"></button>
                      <input type="range" id="seek-bar" value="0">
                    </div>-->

               </div>
            </div>

            

            <footer class="footer-page-logo">
                <div class="page-wrapper">
                    <div class="pos-center">
                        <section class="footer-logos flex-section owl-carousel">

                            <?php echo get_template_part('loops/footer','loop'); ?>

                        </section>
                    </div>
                </div>
            </footer>  
            

        </div>



<?php get_footer(); ?>
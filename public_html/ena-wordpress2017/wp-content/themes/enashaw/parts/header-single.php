
<?php

  $hero_banner = get_field('top_hero_single');

    if($hero_banner) {
      echo wp_get_attachment_image($hero_banner);
    }

 ?>

<header class="header-page">

        <div class="hero-banner">
            <div class="mask-img"></div>


            <div class="absolute-page-bck bcg blog-single-bck" style="background-image: url('<?php echo $hero_banner; ?>');"></div>
        </div>

        <div class="badger-header-page">

            <div class="blog-single-wrapper">
               <div class="content-center">
                    <div class="btn-header">
                        <div class="header-flex">
                            <h2 class="page-header">News</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</header>
<header class="header-page-blog">

                <div class="hero-banner-blog">
                    <div class="mask-img"></div>
                    <div class="absolute-page-blog bcg"></div>

                    <div class="bottom-content-blog">
                        <div class="page-wrapper">
                             <div class="pos-center">

                                <div class="row-content">
                                    <div class="badge-section">
                                         <div class="btn-header">
                                            <div class="header-flex">
                                                <h1 class="page-header"><span class="top-badge-text">All</span> News</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    
                               
                                <div class="row-content">
                                    <div class="short-blog-content">
                                        <h2 class="blog-header">
                                            <span class="welcome-text">Welcome to the news section</span>
                                            <span class="switcher-text"><i class="fa fa-chevron-down" aria-hidden="true"></i></span>
                                            
                                         </h2>
                                          
                                    </div>
                                </div>
                               
                             </div>
                        </div><!--End of page wrapper-->
                    </div>
                </div>
            </header>
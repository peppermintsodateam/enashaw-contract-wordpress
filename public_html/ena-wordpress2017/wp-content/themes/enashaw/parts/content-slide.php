<?php 

        $text = get_field('slide_content'); 
 
    ?>


<section class="slide-content">
            <section class="slide-content-inner">
                <div class="slide-key-message">
                    <h3 class="slide-sub-header yellow-header"><?php the_title(); ?></h3>
                    <h1 class="key-slide-header white-header">
                        <?php echo $text; ?>
                    </h1>
                </div>
            </section>
        </section>
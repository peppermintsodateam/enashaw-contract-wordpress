  <?php 

        $text = get_field('slide_content'); 
        $btn_text = get_field('ctn_btn'); 
        $btn_url = get_field('cta_btn_url');

        $second_btn_url  = get_field('cta_second_btn');
        $second_btn_text  = get_field('cta_second_btn_text');
    ?>

    <section class="slide-content">
            <section class="slide-content-inner">
                <div class="slide-key-message">
                    <h3 class="slide-sub-header yellow-header"><?php the_title(); ?></h3>
                    <h1 class="key-slide-header white-header">
                        <?php echo $text; ?>
                    </h1>

                    <a href="<?php echo $btn_url ?>" class="cta-btn read-more"><?php echo $btn_text; ?></a>
                    <a href="<?php echo $second_btn_url ?>" class="cta-btn read-more margin-btn"><?php echo $second_btn_text; ?></a>
                </div>
            </section>
        </section>
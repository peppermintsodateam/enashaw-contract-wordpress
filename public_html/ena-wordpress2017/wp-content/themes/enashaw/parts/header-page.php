<header class="header-page">

        <div class="hero-banner">
            <div class="mask-img"></div>

            <?php if(has_post_thumbnail()) {
                    $feat_image = wp_get_attachment_url(
                        get_post_thumbnail_id($post->ID)
                    );

                } else {
                        $feat_image = get_field('default_featured_image', 'option');
                    } ?>

            <div class="absolute-page-bck bcg" style="background-image: url(<?php echo $feat_image; ?>);"></div>
        </div>

        <div class="badger-header-page">

            <div class="page-wrapper">
                <div class="row-content">
                    <div class="pos-center">
                        <div class="btn-header">
                            
                            <h1 class="page-header"><span><?php the_title(); ?></span></h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

</header>
<?php 
	add_action('init', 'ena_init_posttypes');

	function ena_init_posttypes() {



		/*Home slider*/
		$labels = array(
			'name'=>__('Home slider'),
			'singular_name'=>__('Home slider'),
			'add_new'=>__('Add new home slide'),
			'add_new_item'=>__('Add new home slide'),
			'edit_item'=>__('Edit home slide'),
			'new_item'=>__('New home slide'),
			'view_item'=>__('View home slide'),
			'search_items'=>__('Search home slide'),
			'not_found'=>__('No home slide found'),
			'not_found_in_trash' =>__('No home slide found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('home_slider', $args);


		/*Home slider*/
		$labels = array(
			'name'=>__('Social icons'),
			'singular_name'=>__('Social icons'),
			'add_new'=>__('Add new social icon'),
			'add_new_item'=>__('Add new social icon'),
			'edit_item'=>__('Edit social icon'),
			'new_item'=>__('New social icon'),
			'view_item'=>__('View social icon'),
			'search_items'=>__('Search social icon'),
			'not_found'=>__('No social icon found'),
			'not_found_in_trash' =>__('No social icon found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('social_icons', $args);



		/*Case studies*/

		$labels = array(
			'name'=>__('Hospitality cases'),
			'singular_name'=>__('hospitality case'),
			'add_new'=>__('Add new hospitality case'),
			'add_new_item'=>__('Add new hospitality case'),
			'edit_item'=>__('Edit hospitality case'),
			'new_item'=>__('New hospitality case'),
			'view_item'=>__('View hospitality case'),
			'search_items'=>__('Search hospitality case'),
			'not_found'=>__('No case hospitality found'),
			'not_found_in_trash' =>__('No case hospitality found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('hospitality_case', $args);


		
		$labels = array(
			'name'=>__('Housing cases'),
			'singular_name'=>__('housing case'),
			'add_new'=>__('Add new housing case'),
			'add_new_item'=>__('Add new housing case'),
			'edit_item'=>__('Edit housing case'),
			'new_item'=>__('New housing case'),
			'view_item'=>__('View housing case'),
			'search_items'=>__('Search housing case'),
			'not_found'=>__('No housing case found'),
			'not_found_in_trash' =>__('No case housing found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('housing_case', $args);


		$labels = array(
			'name'=>__('Education cases'),
			'singular_name'=>__('education case'),
			'add_new'=>__('Add new education case'),
			'add_new_item'=>__('Add new education case'),
			'edit_item'=>__('Edit education case'),
			'new_item'=>__('New education case'),
			'view_item'=>__('View education case'),
			'search_items'=>__('Search education case'),
			'not_found'=>__('No case education found'),
			'not_found_in_trash' =>__('No case education found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('education_case', $args);



		$labels = array(
			'name'=>__('Care cases'),
			'singular_name'=>__('care case'),
			'add_new'=>__('Add new care case'),
			'add_new_item'=>__('Add new care case'),
			'edit_item'=>__('Edit care case'),
			'new_item'=>__('New care case'),
			'view_item'=>__('View care case'),
			'search_items'=>__('Search care case'),
			'not_found'=>__('No care found case'),
			'not_found_in_trash' =>__('No care case found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('care_case', $args);


		$labels = array(
			'name'=>__('Manufacturing cases'),
			'singular_name'=>__('manufacturing case'),
			'add_new'=>__('Add new manufacturing case'),
			'add_new_item'=>__('Add new manufacturing case'),
			'edit_item'=>__('Edit manufacturing case'),
			'new_item'=>__('New manufacturing case'),
			'view_item'=>__('View manufacturing case'),
			'search_items'=>__('Search manufacturing case'),
			'not_found'=>__('No case manufacturing found'),
			'not_found_in_trash' =>__('No case manufacturing found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('manufacturing_case', $args);




		/*Footer bottom logos set*/
		$labels = array(
			'name'=>__('Footer logos'),
			'singular_name'=>__('footer logo'),
			'add_new'=>__('Add new footer logo'),
			'add_new_item'=>__('Add new footer logo'),
			'edit_item'=>__('Edit footer logo'),
			'new_item'=>__('New footer logo'),
			'view_item'=>__('View footer logo'),
			'search_items'=>__('Search footer logo'),
			'not_found'=>__('No footer logo found'),
			'not_found_in_trash' =>__('No footer logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('footer_logo', $args);



		$labels = array(
			'name'=>__('Testimonials'),
			'singular_name'=>__('testimonials'),
			'add_new'=>__('Add new testimonial'),
			'add_new_item'=>__('Add new testimonials'),
			'edit_item'=>__('Edit testimonials'),
			'new_item'=>__('New testimonials'),
			'view_item'=>__('View testimonials'),
			'search_items'=>__('Search testimonials'),
			'not_found'=>__('No testimonials found'),
			'not_found_in_trash' =>__('No testimonials found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),

			'taxonomies'=>array('testimonials_category'),
		);

		register_post_type('testimonials_ena', $args);

		

		/*Videos */
		$labels = array(
			'name'=>__('Videos ena'),
			'singular_name'=>__('Video ena'),
			'add_new'=>__('Add new videos ena'),
			'add_new_item'=>__('Add new videos ena'),
			'edit_item'=>__('Edit videos ena'),
			'new_item'=>__('New videos ena'),
			'view_item'=>__('View videos ena'),
			'search_items'=>__('Search videos ena'),
			'not_found'=>__('No videos ena'),
			'not_found_in_trash' =>__('No videos ena found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			),
		);

		register_post_type('videos_ena', $args);


		/*Footer logos*/

		$labels = array(
			'name'=>__('Footer logo'),
			'singular_name'=>__('footer logo'),
			'add_new'=>__('Add new footer logo'),
			'add_new_item'=>__('Add new footer logo'),
			'edit_item'=>__('Edit footer logo'),
			'new_item'=>__('New footer logo'),
			'view_item'=>__('View footer logo'),
			'search_items'=>__('Search footer logo'),
			'not_found'=>__('No footer logo'),
			'not_found_in_trash' =>__('No footer logo found in trash')
		);

	$args = array(
			'labels'=>$labels,
			'has_archive'=>true,
			'public' => true,
			'hierarchical'=>false,
			'menu_position'=>5,
			'supports'=>array(
				'title',
				'editor',
				'excerpt',
				'thumbnail',
				'page-attributes'
			)
		);

		register_post_type('footer_logo', $args);

	}

/*TAXONOMIES*/
add_action('init','ena_init_taxonomies');

function ena_init_taxonomies() {

	register_taxonomy('testimonials_category', 'testimonials_ena',
			array(
				'labels'=>array(
					'name' => 'Categories',
					'singular_name'=>'Category',
					'search_items'=>'Search Categories',
					'all_items'=>'All Categories',
					'edit_item'=>'Edit Category',
					'update_item'=>'Update Category',
					'add_new_item'=>'Add New Category',
					'new_item_name'=>'New Category Name',
					'menu_name'=>'Categories'
				),

				'hierarchical'=>true,
				'sort'=>true,
				'args'=>array('orderby'=>'term_order'),
				'rewrite'=> true,
				'show_admin_column'=>true
			)

		);

}


?>
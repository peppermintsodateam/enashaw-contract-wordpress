<?php get_header(); ?>
<?php the_post(); ?>

 <?php if ( has_post_thumbnail() ) {

      // Get the post thumbnail URL
      $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

        } else {
      
        // Get the default featured image in theme options
        $feat_image = get_field('default_featured_image', 'option');
} ?>


<div class="main-page">
	 <?php get_template_part('parts/header','single'); ?>

	 <section id="single-page-content" class="section">
              
                    <div class="content-center">

                        <section id="blog-content">

                            <header class="blog-single-header">
                                <h1 class="blog-header">
                                    <?php the_title(); ?>
                                </h1>
                            </header>

                         	<?php 
                         		the_content();
                         	 ?>

                          

                            <div class="bottom-navigation-wrapper">
                                <?php previous_post_link( '%link', 'Previous post', TRUE ); ?>
                  				<?php next_post_link( '%link', 'Next post', TRUE ); ?>
                            </div>

                            <section class="related-posts-wrapper">

                                <header class="blog-single-header">
	                                <h2 class="blog-header">
	                                   Related articles
	                                </h2>
                            	</header>

	                            <div class="related-container">
									<?php
										$custom_taxterms = wp_get_object_terms( $post->ID, 'category', array('fields' => 'ids') );


										$args = array(
											'post_type'=>'post',
											'post_status' => 'publish',
											'posts_per_page' => 2,
											'post__not_in' => array ( $post->ID ),
											'tax_query' => array(
													array(
                                						'taxonomy' => 'category',
                                						'field' => 'id',
                                						'terms' => $custom_taxterms
                            						)
											)
										);

										$related_items = new WP_Query( $args );

										if ( $related_items->have_posts() ) : ?>

											 <?php while ( $related_items->have_posts() ) : $related_items->the_post(); 
											 		$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
											 ?>

												<article class="blog-article">
													<div class="article-inner-blog">
				                                        <div class="blog-article-content bck bck-f" style="background-image: url(<?php echo $image; ?>);">
				                                            <div class="absolute-inner">
				                                                <div class="blog-flex-wrapper">
				                                                    <div class="blog-item-wrapper">
				                                                        <h3 class="blog-title"><?php the_title(); ?></h3>
				                                                        <span class="date"><?php echo get_the_date('M jS, Y'); ?></span>
				                                                        <a href="<?php the_permalink(); ?>" class="cta-blog read-more">Read more</a>
				                                                    </div>
				                                                </div>
				                                            </div>
				                                        </div>
                                    				</div>
												</article>

											 <?php endwhile; ?>

										<?php endif;
                            				// Reset Post Data
                            			wp_reset_postdata();
									 ?>
	                            </div>

                            </section>

                        </section>

                    </div>
              
            </section>
</div>


<?php get_footer(); ?>
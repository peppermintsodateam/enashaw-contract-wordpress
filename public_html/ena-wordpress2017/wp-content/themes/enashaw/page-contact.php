<?php 
/*
    Template Name: Contact Page
*/
?>

<?php get_header(); ?>



	<div class="main-page">

	 <?php get_template_part('parts/header','page'); ?>

            <section id="contact" class="section">
                <div class="page-wrapper">
                   <div class="pos-center">

                     <div class="row-content">
                         <div class="contact-container">

                               <div class="contact-details-column">
                                    <h2 class="contact-header">Contact us</h2>

                                     <div class="top-paragraph">
                                      <?php if(have_posts() ) : ?>

                                        <?php while(have_posts() ) : the_post(); ?>
                                            <?php the_content(); ?>
                                        <?php endwhile; ?>

                                      <?php endif; ?>
                                     </div>

                                     <div class="contact-details-wrapper">
                                       <?php if(have_rows('contact_details') ) : ?>

                                           <?php while(have_rows('contact_details') ) : the_row(); ?>
                                             <p class="contact-list-el"><?php the_sub_field('icon'); ?><?php the_sub_field('text') ?> <a target="<?php the_sub_field('target'); ?>" href="<?php the_sub_field('url_link'); ?>"><span class="<?php the_sub_field('link_class'); ?>"><?php the_sub_field('text_link'); ?></span></a> </p>
                                           <?php endwhile; ?>

                                           

                                       <?php endif; ?>
                                       
                                     </div>

                                        <div class="ena-address-box">

                                           <h3 class="inner-header">
                                             Address:
                                           </h3>

                                           <p>
                                              <?php the_field('address_details'); ?>
                                           </p>
                                        </div>

                                            <div class="map-content">
                                                <div id="map" style="width: 100%;"></div>
                                                <a class="map-pin-link" target="_blank" class="map-link" href="https://goo.gl/maps/zZtqA6SUcDL2">Find Us on map</a>
                                            </div>

                               </div>

                           <div class="form-column">
                                <h2 class="contact-header">Get in touch</h2>
                                <?php echo do_shortcode('[contact-form-7 id="10" title="Main contact form" html_id="contact-form" html_class="contact-form"]') ?>
                           </div>

                     </div>
                     </div>

                   </div>

                </div>
            </section>

            

            <footer class="footer-page-logo">
                <div class="page-wrapper">
                    <div class="pos-center">
                       <section class="footer-logos flex-section owl-carousel">

                            <?php echo get_template_part('loops/footer','loop'); ?>

                        </section>
                    </div>
                </div>
            </footer>  
            

        </div>



<script>
    var unrappedEl = jQuery('.contact-list-el').find('.no-link');
    unrappedEl.unwrap();
</script>

<?php get_footer(); ?>
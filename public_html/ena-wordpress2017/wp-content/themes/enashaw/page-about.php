<?php 
/*
	Template Name: About Page
*/
?>

<?php get_header(); ?>

	<div class="main-page">

            <?php get_template_part('parts/header','page'); ?>

            <section id="about" class="section">
                <div class="page-wrapper">
                    
                        
                    <div class="row-content">
                        <section class="about-section who-we-are left-align">
                                <?php get_template_part('loops/loop','who'); ?>
                        </section>

                         <section class="about-section history right-align">
                            <?php get_template_part('loops/loop','history'); ?>
                        </section>

                       
                        <section class="about-section fabric left-align">
                              <?php get_template_part('loops/loop','fabric'); ?>
                        </section>
                    </div> 
                   
                </div>
            </section>

            <footer class="footer-page-logo">
                <div class="page-wrapper">
                    <div class="pos-center">
                        <section class="footer-logos flex-section owl-carousel">

                            <?php echo get_template_part('loops/footer','loop'); ?>

                        </section>
                    </div>
                </div>
            </footer>  
            

        </div>

<?php get_footer(); ?>